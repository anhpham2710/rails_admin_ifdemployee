class CreateEmployees < ActiveRecord::Migration[5.0]
  def change
    create_table :employees do |t|
      t.string :name
      t.string :password
      t.string :role
      t.integer :phone
      t.string :email
      t.string :address
      t.datetime :birthdate
      t.string :skype
      t.boolean :active

      t.timestamps
    end
  end
end
