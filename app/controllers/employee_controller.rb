class EmployeeController < ApplicationController
  skip_before_filter :verify_authenticity_token
  def show
    request_user = Employee.find_by(email: params[:email])
    if request_user.password == params[:password]
      render json: Employee.all, status: 200
    else
      render json: {error: "not found"}, status: 404
    end unless request_user.nil?
  end

end
