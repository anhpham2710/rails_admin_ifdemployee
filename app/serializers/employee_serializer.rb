class EmployeeSerializer < ActiveModel::Serializer
  attributes :id, :name, :password, :role, :skype, :email, :address, :phone, :birthdate, :image
  private

  def birthdate
    object.birthdate.strftime("%Y-%m-%d") unless object.birthdate.nil?
  end

end
